package networkninjas.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import networkninjas.helpers.YoutubeVideoHelper;

@Entity
@Table(name = "youtube_video")
public class YoutubeVideo implements Serializable {
	 @Id
	 @GeneratedValue
	 @Column(name = "youtube_video_id", unique = true, nullable = false)
	private long youtubeVideoId;
	private String name; // youtube name
	private String description; // popis videa v databázi pro určení videa
	private int width;
	private int height;

	// Time in seconds when to start playing video
	private int startAt;

	private boolean isShowRelated;
	private boolean isShowControls;
	// Cookies are not sent unless video is played
	private boolean isPrivacyPolicy;
	private boolean isShowInfo;
	private boolean isAutoplay;

	private boolean isCaption;
	// Language prefix according to ISO 639-1 standard
	private String captionLanguage;
	private boolean isAllowFullscreen;

	public static final int DEFAULT_WIDTH = 560;
	public static final int DEFAULT_HEIGHT = 315;
	public static final double CONVERT_COEFICIENT = new Integer(DEFAULT_HEIGHT).doubleValue() / new Integer(DEFAULT_WIDTH).doubleValue();
	private static final String URL_DELIMITER = "&amp;";
	private static final String URL_TIME_DELIMITER = "#";
	private static final String URL_YOUTUBE_BASE_START = "https://www.youtube";
	private static final String URL_YOUTUBE_BASE_END = ".com/embed/";
	private static final String URL_YOUTUBE_NO_COOKIE = "-nocookie";
	private static final String URL_ALLOW_FULLSCREEN = "allowfullscreen";

	// used to save parameters for building URL
	// !!! Need to call setter or better call in some init function
	private String params;
	private String timeString;
	private String src;
	private String allowFullscreen;

	// calculated according to ratio
	private int properWidth;
	private int properHeight;

	// mn
/*	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "youtube_playlist_youtube_video", joinColumns = {
			@JoinColumn(name = "youtube_video_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "youtube_playlist_id", nullable = false, updatable = false) })*/
	private Set<YoutubePlaylist> youtubePlaylists = new HashSet<YoutubePlaylist>(0);

	
	public long getYoutubeVideoId() {
		return youtubeVideoId;
	}

	public void setYoutubeVideoId(long youtubeVideoId) {
		this.youtubeVideoId = youtubeVideoId;
	}

	public void setParams() {
		List<String> paramsList = new LinkedList<String>();

		if (this.isAutoplay) {
			paramsList.add("autoplay=1");
		}

		if (!this.isShowRelated) {
			paramsList.add("rel=0");
		}

		if (!this.isShowControls) {
			paramsList.add("controls=0");
		}

		if (!this.isShowInfo) {
			paramsList.add("showinfo=0");
		}

		if (this.isCaption && !this.captionLanguage.isEmpty()) {
			paramsList.add("cc_load_policy=1");
			paramsList.add("cc_lang_pref=" + this.captionLanguage);
		}

		try {
			this.params = String.join(URL_DELIMITER, paramsList);
		} catch (NullPointerException e) {
			this.params = "";
		}
	}

	public String getParams() {
		return this.params;
	}

	public void setTimeString() {
		if (this.startAt == 0)
			this.timeString = "";
		else {
			this.timeString = YoutubeVideoHelper.timeInSecondsToMinutesAndSeconds(this.startAt);
		}
	}

	public String getTimeString() {
		return this.timeString;
	}

	public void setAllowFullscreen() {
		if (this.isAllowFullscreen)
			this.allowFullscreen = YoutubeVideo.URL_ALLOW_FULLSCREEN;
		else
			this.allowFullscreen = "";
	}

	public String getAllowFullscreen() {
		return this.allowFullscreen;
	}

	public void setSrc() {
		String url = "";
		if (this.name.isEmpty())
			this.src = url;
		else {
			url += YoutubeVideo.URL_YOUTUBE_BASE_START;
			if (this.isPrivacyPolicy)
				url += YoutubeVideo.URL_YOUTUBE_NO_COOKIE;
			url += URL_YOUTUBE_BASE_END;
			url += this.name;
			if (!this.getParams().isEmpty()) {
				url += "?";
				url += this.getParams();
			}

			if (!this.getTimeString().isEmpty()) {
				url += YoutubeVideo.URL_TIME_DELIMITER;
				url += this.getTimeString();
			}
		this.src = url;
		}

	}

	

	public void setProperWidthAndHeight() {
		HashMap<String, Integer> map = YoutubeVideoHelper.getWidthAndHeight(this.getWidth(), this.getHeight());
		setProperWidth(map.get("width"));
		setProperHeight(map.get("height"));
	}

	public Set<YoutubePlaylist> getYoutubePlaylists() {
		return this.youtubePlaylists;
	}

	public void setYoutubePlaylists(Set<YoutubePlaylist> youtubePlaylists) {
		this.youtubePlaylists = youtubePlaylists;
	}
	
	public String getSrc() {
		return this.src;
	}

	public int getHeight() {
		return this.height;
	}

	public int getWidth() {
		return this.width;
	}

	public void setProperWidth(int width) {
		this.properWidth = width;

	}

	public void setProperHeight(int height) {
		this.properHeight = height;

	}

	public int getProperHeight() {
		return this.properHeight;
	}

	public int getProperWidth() {
		return this.properWidth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStartAt() {
		return startAt;
	}

	public void setStartAt(int startAt) {
		this.startAt = startAt;
	}

	public boolean getIsShowRelated() {
		return isShowRelated;
	}

	public void setIsShowRelated(boolean isShowRelated) {
		this.isShowRelated = isShowRelated;
	}

	public boolean getIsShowControls() {
		return isShowControls;
	}

	public void setIsShowControls(boolean isShowControls) {
		this.isShowControls = isShowControls;
	}

	public boolean getIsPrivacyPolicy() {
		return isPrivacyPolicy;
	}

	public void setIsPrivacyPolicy(boolean isPrivacyPolicy) {
		this.isPrivacyPolicy = isPrivacyPolicy;
	}

	public boolean getIsShowInfo() {
		return isShowInfo;
	}

	public void setIsShowInfo(boolean isShowInfo) {
		this.isShowInfo = isShowInfo;
	}

	public boolean getIsAutoplay() {
		return isAutoplay;
	}

	public void setIsAutoplay(boolean isAutoplay) {
		this.isAutoplay = isAutoplay;
	}

	public boolean getIsCaption() {
		return isCaption;
	}

	public void setIsCaption(boolean isCaption) {
		this.isCaption = isCaption;
	}

	public String getCaptionLanguage() {
		return captionLanguage;
	}

	public void setCaptionLanguage(String captionLanguage) {
		this.captionLanguage = captionLanguage;
	}

	public boolean getIsAllowFullscreen() {
		return isAllowFullscreen;
	}

	public void setIsAllowFullscreen(boolean isAllowFullscreen) {
		this.isAllowFullscreen = isAllowFullscreen;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void init() {
		setProperWidthAndHeight();
		setParams();
		setTimeString();
		setSrc();
		setAllowFullscreen();
		
	}

	
}