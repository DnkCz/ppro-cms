package networkninjas.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.ManyToMany;

import javax.persistence.Table;

@Entity
@Table(name = "youtube_playlist")
public class YoutubePlaylist implements Serializable {
	@Id
	@GeneratedValue
	@Column(name = "youtube_playlist_id", unique = true, nullable = false)
	private long youtubePlaylistId;
	
	private String name;
	private String description;
	private Set<YoutubeVideo> youtubeVideos = new HashSet<YoutubeVideo>(0);

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getYoutubePlaylistId() {
		return youtubePlaylistId;
	}

	public void setYoutubePlaylistId(long youtubePlaylistId) {
		this.youtubePlaylistId = youtubePlaylistId;
	}

	
	/*@ManyToMany(fetch = FetchType.LAZY, mappedBy = "youtube_playlists")*/
	public Set<YoutubeVideo> getYoutubeVideos() {
		return this.youtubeVideos;
	}

	public void setYoutubeVideos(Set<YoutubeVideo> youtubeVideos) {
		this.youtubeVideos = youtubeVideos;
	}
	

}
