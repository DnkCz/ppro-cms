package networkninjas.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import networkninjas.model.YoutubePlaylist;

@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class YoutubePlaylistDao {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * @Transactional annotation below will trigger Spring Hibernate transaction
	 *                manager to automatically create a hibernate session. See
	 *                src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@Transactional
	public List<YoutubePlaylist> findAll() {
		Session session = sessionFactory.getCurrentSession();
		List youtubePlaylists = session.createQuery("from YoutubePlaylist").list();
		return youtubePlaylists;
	}

	@Transactional
	public YoutubePlaylist getById(int id) {
		YoutubePlaylist youtubePlaylist = null;
		//System.out.println("ID = "+id);
		Session session = sessionFactory.getCurrentSession();
		List youtubePlaylists = session.createQuery("from YoutubePlaylist WHERE id = '" + id + "'").list();
		if (youtubePlaylists.size() > 0) {
			youtubePlaylist = (YoutubePlaylist) youtubePlaylists.get(0);
			//System.out.println("YoutubePlaylist="+youtubePlaylist);
		}
		return youtubePlaylist;
	}
	
	@Transactional
	public void saveOrUpdate(YoutubePlaylist youtubePlaylist) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(youtubePlaylist);
	}
	
	@Transactional
	public void delete(YoutubePlaylist youtubePlaylist) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(youtubePlaylist);
	}
}
