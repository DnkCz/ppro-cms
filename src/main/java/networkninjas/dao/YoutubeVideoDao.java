package networkninjas.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import networkninjas.model.YoutubeVideo;

@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class YoutubeVideoDao {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * @Transactional annotation below will trigger Spring Hibernate transaction
	 *                manager to automatically create a hibernate session. See
	 *                src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@Transactional
	public List<YoutubeVideo> findAll() {
		Session session = sessionFactory.getCurrentSession();
		List youtubeVideos = session.createQuery("from YoutubeVideo").list();
		return youtubeVideos;
	}

	@Transactional
	public YoutubeVideo getById(int id) {
		YoutubeVideo youtubeVideo = null;
		//System.out.println("ID = "+id);
		Session session = sessionFactory.getCurrentSession();
		List youtubeVideos = session.createQuery("from YoutubeVideo WHERE id = '" + id + "'").list();
		if (youtubeVideos.size() > 0) {
			youtubeVideo = (YoutubeVideo) youtubeVideos.get(0);
			//System.out.println("YoutubeVideo="+article);
		}
		return youtubeVideo;
	}
	
	@Transactional
	public void saveOrUpdate(YoutubeVideo youtubeVideo) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(youtubeVideo);
	}
	
	@Transactional
	public void delete(YoutubeVideo youtubeVideo) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(youtubeVideo);
	}
}
