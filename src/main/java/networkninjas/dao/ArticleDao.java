package networkninjas.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import networkninjas.model.Article;

@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class ArticleDao {
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * @Transactional annotation below will trigger Spring Hibernate transaction
	 *                manager to automatically create a hibernate session. See
	 *                src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@Transactional
	public List<Article> findAll() {
		Session session = sessionFactory.getCurrentSession();
		List articles = session.createQuery("from Article").list();
		return articles;
	}

	@Transactional
	public Article getById(int id) {
		Article article = null;
		//System.out.println("ID = "+id);
		Session session = sessionFactory.getCurrentSession();
		List articles = session.createQuery("from Article WHERE id = '" + id + "'").list();
		if (articles.size() > 0) {
			article = (Article) articles.get(0);
			//System.out.println("Article="+article);
		}
		return article;
	}
	
	@Transactional
	public void saveOrUpdate(Article article) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(article);
		
	}
	
	@Transactional
	public void delete(Article article) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(article);
	}
	
	@Transactional
	public Article getByName(Article article) {
		//System.out.println("ID = "+id);
		Session session = sessionFactory.getCurrentSession();
		List articles = session.createQuery("from Article WHERE name = '" + article.getName() + "'").list();
		if (articles.size() > 0) {
			article = (Article) articles.get(0);
			//System.out.println("Article="+article);
			return article;
		}
		return null;
		
	}
}
