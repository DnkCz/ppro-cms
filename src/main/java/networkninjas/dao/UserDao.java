package networkninjas.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import networkninjas.model.User;

@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {
		Session session = sessionFactory.getCurrentSession();
		List users = session.createQuery("from User where username=?").setParameter(0, username).list();

		if (users.size() > 0) {
			return (User) users.get(0);
		} else {
			return null;
		}

	}
}
