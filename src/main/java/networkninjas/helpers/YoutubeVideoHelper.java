package networkninjas.helpers;

import java.util.HashMap;
import java.util.Map;

import networkninjas.model.YoutubeVideo;

public class YoutubeVideoHelper {
	private static final String MINUTES_CHAR = "m";
	private static final String SECONDS_CHAR = "s";
	private static final String URL_FRAMEBORDER = "frameborder=\"0\"";

	public static String getTemplate(YoutubeVideo video) {

		String template = "<iframe width=\"" + video.getProperWidth() + "\" height=\"" + video.getProperHeight()
				+ "\" src=\"" + video.getSrc() + " \" " + URL_FRAMEBORDER + " " + video.getAllowFullscreen()
				+ "></iframe>";

		return template;
	}

	public static String timeInSecondsToMinutesAndSeconds(int seconds) {
		int mins = seconds / 60;
		int secs = seconds % 60;

		return mins + MINUTES_CHAR + secs + SECONDS_CHAR;
	}

	public static HashMap<String, Integer> getWidthAndHeight(int width, int height) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		if ((width == 0) && (height == 0)) {
			map.put("width", YoutubeVideo.DEFAULT_WIDTH);
			map.put("height", YoutubeVideo.DEFAULT_HEIGHT);
			return map;
		}
		if (width == 0) {
			int tmpWidth = new Double((1 / YoutubeVideo.CONVERT_COEFICIENT) * height).intValue();
			map.put("width", tmpWidth);
			map.put("height", height);
			return map;
		}
		if (height == 0) {
			// DB DEBUG
			//System.out.println("JSEM TU");
			//System.out.println("VALUE= "+new Double(0.5*width).intValue());
			int tmpHeight = new Double((YoutubeVideo.CONVERT_COEFICIENT) * width).intValue();
			map.put("width", width);
			map.put("height", tmpHeight);
			return map;
		}

		// force calculation ration according to width 
		int tmpHeight = new Double((YoutubeVideo.CONVERT_COEFICIENT) * height).intValue();
		map.put("width", width);
		map.put("height", tmpHeight);
		return map;

	}

	
	
}
