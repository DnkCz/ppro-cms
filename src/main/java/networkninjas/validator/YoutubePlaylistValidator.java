package networkninjas.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import networkninjas.model.YoutubePlaylist;

public class YoutubePlaylistValidator implements Validator {

	private static String NAME_EMPTY = "name is empty";

	@Override
	public boolean supports(Class<?> clazz) {
		YoutubePlaylist.class.equals(clazz);
		return false;
	}

	@Override
	public void validate(Object obj, Errors e) {
		ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
		YoutubePlaylist youtubePlaylist = (YoutubePlaylist) obj;
		if (youtubePlaylist.getName().isEmpty()) {
			e.rejectValue("name", NAME_EMPTY);
		}

	}

}
