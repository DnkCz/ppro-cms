package networkninjas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import networkninjas.dao.YoutubeVideoDao;
import networkninjas.helpers.YoutubeVideoHelper;
import networkninjas.model.Article;
import networkninjas.model.YoutubeVideo;
import networkninjas.validator.YoutubeVideoValidator;


@Controller
@Configuration
@ComponentScan("networkninjas.dao")
public class YoutubeVideoController {

	@Autowired
	private YoutubeVideoDao youtubeVideoDAO;

	/**
	 * This handler method is invoked when http://localhost:8080/pizzashop is
	 * requested. The method returns view name "index" which will be resolved
	 * into /WEB-INF/index.jsp. See src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@RequestMapping(value = "/youtubeVideos/")
	public ModelAndView list() {
		List<YoutubeVideo> youtubeVideos = youtubeVideoDAO.findAll();
		return new ModelAndView("youtubeVideo_list","youtubeVideos",youtubeVideos);
	}

	@RequestMapping(value = "/youtubeVideos/{youtubeVideoID}")
	public ModelAndView findYoutubeVideo(@PathVariable String youtubeVideoID, Model model) {
		try {
			int youtubeVideoid = Integer.parseInt(youtubeVideoID);
			YoutubeVideo youtubeVideo = youtubeVideoDAO.getById(youtubeVideoid);
			if (youtubeVideo != null){
				model.addAttribute("youtubeVideo", youtubeVideo);
				youtubeVideo.init();
				model.addAttribute("iframe",YoutubeVideoHelper.getTemplate(youtubeVideo));
			}
			else{
				model.addAttribute("youtubeVideo", null);
				model.addAttribute("iframe","");
			}
			return new ModelAndView("youtubeVideo_detail", "youtubeVideo", youtubeVideo);
			

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubeVideo", null);
			return new ModelAndView("youtubeVideo_list");
		}

	}
	
	@RequestMapping(value = "/youtubeVideos/{youtubeVideoID}/detail")
	public ModelAndView detailYoutubeVideo(@PathVariable String youtubeVideoID, Model model) {
		try {
			int youtubeVideoid = Integer.parseInt(youtubeVideoID);
			YoutubeVideo youtubeVideo = youtubeVideoDAO.getById(youtubeVideoid);
			if (youtubeVideo != null){
				model.addAttribute("youtubeVideo", youtubeVideo);
				youtubeVideo.init();
				model.addAttribute("iframe",YoutubeVideoHelper.getTemplate(youtubeVideo));
			}
			else{
				model.addAttribute("youtubeVideo", null);
				model.addAttribute("iframe","");
			}
			return new ModelAndView("youtubeVideo_detail", "youtubeVideo", youtubeVideo);
			

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubeVideo", null);
			return new ModelAndView("youtubeVideo_list");
		}
	}

	
	@RequestMapping(value = "/admin/youtubeVideos/{youtubeVideoID}/edit")
	public ModelAndView editYoutubeVideo(@PathVariable String youtubeVideoID, Model model) {
		try {
			int youtubeVideoid = Integer.parseInt(youtubeVideoID);
			YoutubeVideo youtubeVideo = youtubeVideoDAO.getById(youtubeVideoid);
			if (youtubeVideo != null) {
				return new ModelAndView("/admin/youtubeVideo_edit", "youtubeVideo", youtubeVideo);
			}
			return new ModelAndView("/admin/youtubeVideo_list");

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubeVideo", null);
			return new ModelAndView("/admin/youtubeVideo_list");
		}

	}
	
	@RequestMapping(value = "/admin/youtubeVideos/insert")
	public ModelAndView insertYoutubeVideo() {
		return new ModelAndView("/admin/youtubeVideo_edit", "youtubeVideo", new YoutubeVideo());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/admin/saveYoutubeVideo")
	public ModelAndView saveYoutubeVideo(@ModelAttribute("youtubeVideo") YoutubeVideo youtubeVideo, BindingResult result) {


		new YoutubeVideoValidator().validate(youtubeVideo, result);
		if (result.hasErrors()) {
			return new ModelAndView("/admin/youtubeVideo_detail", "youtubeVideo", youtubeVideo);
		}
		
		
		this.youtubeVideoDAO.saveOrUpdate(youtubeVideo);
		return listAdmin();

	}
	
	
	
	@RequestMapping(value = "/admin/youtubeVideos/{youtubeVideoID}/delete")
	public ModelAndView deleteYoutubeVideo(@PathVariable String youtubeVideoID, Model model) {
		try {
			int youtubeVideoid = Integer.parseInt(youtubeVideoID);
			YoutubeVideo youtubeVideo = youtubeVideoDAO.getById(youtubeVideoid);
			if (youtubeVideo != null) {
				this.youtubeVideoDAO.delete(youtubeVideo);
				// TODO message do šablony
				//return this.list();
			}
			//return this.list();

			
		} catch (NumberFormatException e) {
			//return this.list();
		}
		return this.listAdmin();
	}
	
	@RequestMapping(value = "/admin/youtubeVideos/")
	public ModelAndView listAdmin() {
		List<YoutubeVideo> videos = youtubeVideoDAO.findAll();
		return new ModelAndView("/admin/youtubeVideo_list","youtubeVideos",videos);
	}
	
	
	
	

}
