package networkninjas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import networkninjas.dao.ArticleDao;
import networkninjas.model.Article;
import networkninjas.validator.ArticleValidator;

@Controller
@Configuration
@ComponentScan("networkninjas.dao")
public class ArticleController {

	@Autowired
	private ArticleDao articleDAO;

	/**
	 * This handler method is invoked when http://localhost:8080/pizzashop is
	 * requested. The method returns view name "index" which will be resolved
	 * into /WEB-INF/index.jsp. See src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/articles/")
	public ModelAndView list() {
		List<Article> articles = articleDAO.findAll();
		return new ModelAndView("article_list","articles",articles);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/articles/{articleID}")
	public ModelAndView findArticle(@PathVariable String articleID, Model model) {
		try {
			int articleid = Integer.parseInt(articleID);
			Article article = articleDAO.getById(articleid);
			if (article != null)
				model.addAttribute("article", article);
			else
				model.addAttribute("article", null);
			return new ModelAndView("article_detail", "article_detail", article);

			
		} catch (NumberFormatException e) {
			model.addAttribute("article", null);
			return new ModelAndView("article_list");
		}

	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/articles/{articleID}/detail")
	public ModelAndView detailArticle(@PathVariable String articleID, Model model) {
		try {
			int articleid = Integer.parseInt(articleID);
			Article article = articleDAO.getById(articleid);
			if (article != null) {
				return new ModelAndView("article_detail", "article", article);
			}
			return new ModelAndView("article_list");

			
		} catch (NumberFormatException e) {
			model.addAttribute("article", null);
			return new ModelAndView("article_list");
		}

	}

	@RequestMapping(method = RequestMethod.GET, value = "/admin/articles/")
	public ModelAndView listAdmin() {
		List<Article> articles = articleDAO.findAll();
		return new ModelAndView("/admin/article_list","articles",articles);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/admin/articles/{articleID}/edit")
	public ModelAndView editArticle(@PathVariable String articleID, Model model) {
		try {
			int articleid = Integer.parseInt(articleID);
			Article article = articleDAO.getById(articleid);
			if (article != null) {
				return new ModelAndView("/admin/article_edit", "article", article);
			}
			return new ModelAndView("/admin/article_list");

			
		} catch (NumberFormatException e) {
			model.addAttribute("article", null);
			return new ModelAndView("/admin/article_list");
		}

	}
	
	@RequestMapping(method = RequestMethod.GET,value = "/admin/articles/insert")
	public ModelAndView insertArticle() {
		return new ModelAndView("/admin/article_edit", "article", new Article());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/admin/saveArticle")
	public ModelAndView saveArticle(@ModelAttribute("article") Article article, BindingResult result) {

		new ArticleValidator().validate(article, result);
		if (result.hasErrors()) {
			return new ModelAndView("/admin/article_detail", "article", article);
		}
		
		this.articleDAO.saveOrUpdate(article);
		return listAdmin();

	}
	

	
	@RequestMapping(method = RequestMethod.GET, value = "/admin/articles/{articleID}/delete")
	public ModelAndView deleteArticle(@PathVariable String articleID, Model model) {
		try {
			int articleid = Integer.parseInt(articleID);
			Article article = articleDAO.getById(articleid);
			if (article != null) {
				this.articleDAO.delete(article);
				// TODO message do šablony
				//return this.list();
			}
			//return this.list();

			
		} catch (NumberFormatException e) {
			//return this.list();
		}
		return this.listAdmin();
	}
	
	
	
	

}
