package networkninjas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import networkninjas.dao.YoutubePlaylistDao;
import networkninjas.model.YoutubePlaylist;
import networkninjas.validator.YoutubePlaylistValidator;

@Controller
@Configuration
@ComponentScan("networkninjas.dao")
public class YoutubePlaylistController {

	@Autowired
	private YoutubePlaylistDao youtubePlaylistDAO;

	/**
	 * This handler method is invoked when http://localhost:8080/pizzashop is
	 * requested. The method returns view name "index" which will be resolved
	 * into /WEB-INF/index.jsp. See src/main/webapp/WEB-INF/servlet-context.xml
	 */
	@RequestMapping(value = "/youtubePlaylists/")
	public ModelAndView list() {
		List<YoutubePlaylist> youtubePlaylists = youtubePlaylistDAO.findAll();
		return new ModelAndView("youtubePlaylist_list","youtubePlaylists",youtubePlaylists);
	}

	@RequestMapping(value = "/youtubePlaylists/{youtubePlaylistID}")
	public ModelAndView findYoutubePlaylist(@PathVariable String youtubePlaylistID, Model model) {
		try {
			int youtubePlaylistId = Integer.parseInt(youtubePlaylistID);
			YoutubePlaylist youtubePlaylist = youtubePlaylistDAO.getById(youtubePlaylistId);
			if (youtubePlaylist != null)
				model.addAttribute("youtubePlaylist", youtubePlaylist);
			else
				model.addAttribute("youtubePlaylist", null);
			return new ModelAndView("youtubePlaylist_detail", "youtubePlaylist_detail", youtubePlaylist);

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubePlaylist", null);
			return new ModelAndView("youtubePlaylist_list");
		}

	}

	
	@RequestMapping(value = "/youtubePlaylists/{youtubePlaylistID}/edit")
	public ModelAndView editYoutubePlaylist(@PathVariable String youtubePlaylistID, Model model) {
		try {
			int youtubePlaylistId = Integer.parseInt(youtubePlaylistID);
			YoutubePlaylist youtubePlaylist = youtubePlaylistDAO.getById(youtubePlaylistId);
			if (youtubePlaylist != null) {
				return new ModelAndView("youtubePlaylist_edit", "youtubePlaylist", youtubePlaylist);
			}
			return new ModelAndView("youtubePlaylist_list");

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubePlaylist", null);
			return new ModelAndView("youtubePlaylist_list");
		}

	}
	
	@RequestMapping(value = "/youtubePlaylists/insert")
	public ModelAndView insertYoutubePlaylist() {
		return new ModelAndView("youtubePlaylist_edit", "youtubePlaylist", new YoutubePlaylist());
	}

	@RequestMapping(value = "/saveYoutubePlaylist")
	public ModelAndView saveYoutubePlaylist(@ModelAttribute("youtubePlaylist") YoutubePlaylist youtubePlaylist, BindingResult result) {

		new YoutubePlaylistValidator().validate(youtubePlaylist, result);
		if (result.hasErrors()) {
			return new ModelAndView("youtubePlaylist_detail", "youtubePlaylist", youtubePlaylist);
		}
		
		this.youtubePlaylistDAO.saveOrUpdate(youtubePlaylist);
		return list();

	}
	
	@RequestMapping(value = "/youtubePlaylists/{youtubePlaylistID}/detail")
	public ModelAndView detailYoutubePlaylist(@PathVariable String youtubePlaylistID, Model model) {
		try {
			int youtubePlaylistId = Integer.parseInt(youtubePlaylistID);
			YoutubePlaylist youtubePlaylist = youtubePlaylistDAO.getById(youtubePlaylistId);
			if (youtubePlaylist != null) {
				return new ModelAndView("youtubePlaylist_detail", "youtubePlaylist", youtubePlaylist);
			}
			return new ModelAndView("youtubePlaylist_list");

			
		} catch (NumberFormatException e) {
			model.addAttribute("youtubePlaylist", null);
			return new ModelAndView("youtubePlaylist_list");
		}

	}
	
	@RequestMapping(value = "/youtubePlaylists/{youtubePlaylistID}/delete")
	public ModelAndView deleteYoutubePlaylist(@PathVariable String youtubePlaylistID, Model model) {
		try {
			int youtubePlaylistId = Integer.parseInt(youtubePlaylistID);
			YoutubePlaylist youtubePlaylist = youtubePlaylistDAO.getById(youtubePlaylistId);
			if (youtubePlaylist != null) {
				this.youtubePlaylistDAO.delete(youtubePlaylist);
				// TODO message do šablony
				//return this.list();
			}
			//return this.list();

			
		} catch (NumberFormatException e) {
			//return this.list();
		}
		return this.list();
	}
	
	
	
	

}
