<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">List of all articles</div>
  <div class="panel-body">
    <p>This is the list of articles</p>
    <p><a href="/networkninjas/admin/articles/insert" class="btn btn-primary" role="button">Add article</a> </p>
  </div>

  <!-- Table -->
  <table class="table">
  	<tr> <td> ID </td> <td> Name </td> <td> Action </td> </tr>
  	<c:forEach var="a" items="${articles}">
    	<tr> <td> ${a.articleId} </td> <td> ${a.name} </td> <td> <a href="/networkninjas/admin/articles/${a.articleId}/edit"> Edit</a> <a href="/networkninjas/admin/articles/${a.articleId}/delete"> Delete</a> </td> </tr>
    </c:forEach>
  </table>
</div>