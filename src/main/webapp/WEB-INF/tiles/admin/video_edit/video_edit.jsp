<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	

	<form:form action="/networkninjas/admin/saveYoutubeVideo" method="post"
		commandName="youtubeVideo" class="form-inline">
		<c:if test="${youtubeVideo.youtubeVideoId != 0}">
		<div class="form-group">
			<form:input path="youtubeVideoId" class="form-control" value="${youtubeVideo.youtubeVideoId}" style="display:none" />
		 </div>
		</c:if>
		
		<div class="form-group">
			<label for="name">Name:</label>
			<form:input path="name" class="form-control" />
		 </div>
		
		<div class="form-group">
			<label for="description">Description:</label>
			<form:input path="description" class="form-control"/>
		 </div>
		
		<button type="submit" class="btn btn-default">Submit</button>
	</form:form>
