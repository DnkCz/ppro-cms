<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">List of all videos</div>
  <div class="panel-body">
    <p>This is the list of videos</p>
    <p><a href="/networkninjas/admin/youtubeVideos/insert" class="btn btn-primary" role="button">Add video</a> </p>
  </div>

  <!-- Table -->
  <table class="table">
  	<tr> <td> ID </td> <td> Name </td> <td> Description </td> <td> Action </td> </tr>
  	<c:forEach var="v" items="${youtubeVideos}">
    	<tr> <td> ${v.youtubeVideoId} </td> <td> ${v.name} </td> <td> ${v.description} </td> <td> <a href="/networkninjas/admin/youtubeVideos/${v.youtubeVideoId}/edit"> Edit</a> <a href="/networkninjas/admin/youtubeVideos/${v.youtubeVideoId}/delete"> Delete</a> </td> </tr>
    </c:forEach>
  </table>
</div>