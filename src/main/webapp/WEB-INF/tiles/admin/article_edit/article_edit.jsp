<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<h1>Article - ${article.name}</h1>

	<form:form action="/networkninjas/admin/saveArticle" method="post"
		commandName="article" class="form-inline">
		<c:if test="${article.articleId != null}">
		<div class="form-group">
			<form:input path="articleId" class="form-control" value="${article.articleId}" style="display:none" />
		 </div>
		</c:if>
		
		<div class="form-group">
			<label for="name">Name:</label>
			<form:input path="name" class="form-control" />
		 </div>
		
		<div class="form-group">
			<label for="annotation">Annotation:</label>
			<form:input path="annotation" class="form-control"/>
		 </div>
		
		<div class="form-group">
			<label for="description">Description:</label>
			<form:input path="description" class="form-control"/>
		 </div>
		
		<button type="submit" class="btn btn-default">Submit</button>
	</form:form>
