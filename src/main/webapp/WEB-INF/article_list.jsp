<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <h1>List of All Articles</h1>
    <ul>
        <c:forEach var="a" items="${articles}">
            <li> <a href="/networkninjas/articles/${a.articleId}/detail" title="${a.name}">${a.articleId} - ${a.name} </a> </li>
        </c:forEach>
    </ul>
