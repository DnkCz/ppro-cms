-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2017 at 04:59 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `networkninjas`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `annotation` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`article_id`, `name`, `annotation`, `description`) VALUES
(1, 'testttttt', 'test anotaceccc', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent aliquet venenatis iaculis. Nam quis feugiat sem. Duis faucibus leo at rutrum tempus. Proin quam libero, viverra non tempus at, venenatis sit amet quam. Suspendisse ex lorem, commodo nec lorem rutrum, imperdiet tincidunt metus. Vestibulum mollis molestie nibh eget placerat. Ut euismod arcu id odio fermentum lobortis. Curabitur et nisl vel felis auctor tincidunt. Sed eget molestie felis.  Aenean a est consequat nibh volutpat sodales. Ut tristique sed justo in egestas. Nam et tellus sit amet elit pharetra euismod vel luctus mauris. Nam nibh metus, mattis in ornare at, mollis id ex. Etiam laoreet, risus quis sollicitudin viverra, lacus nisi volutpat nisl, eu ultrices nunc diam non ante. Suspendisse dignissim porttitor magna vel convallis. Nulla a metus ac quam pulvinar imperdiet non eu dolor. Integer sed risus non massa rhoncus eleifend. In tempor ante sed lectus fermentum feugiat. Vestibulum eu lacus ac augue mollis eleifend. Pellentesque venenatis maximus lacus, sit amet aliquet arcu pulvinar sit amet. Pellentesque in tempus dui. Sed at libero quis dolor molestie hendrerit. Vestibulum scelerisque sit amet sem ac commodo.  Maecenas fermentum feugiat semper. Etiam lobortis, velit tristique pulvinar luctus, magna est semper nisl, vitae bibendum magna nulla et sem. In ac augue mauris. Aenean placerat eros in luctus suscipit. Morbi quis mi lacus. Praesent volutpat sapien ac gravida sodales. Pellentesque aliquam urna urna, ut malesuada augue porta at. Morbi nec ornare elit, a sollicitudin lorem. Quisque et elit consequat, ultrices enim sit amet, interdum sapien. Praesent vulputate dui ac velit iaculis luctus. Morbi fringilla risus quis volutpat malesuada. Praesent tincidunt nulla et turpis auctor mattis. Etiam justo arcu, pulvinar quis nisl quis, convallis efficitur lorem. Quisque nec eros in nunc venenatis imperdiet vel et enim. In tempus eros quam, ac pharetra dui malesuada suscipit. Fusce gravida leo mauris.  Curabitur maximus nisi vitae felis mollis, ac feugiat tellus mattis. Ut blandit lorem libero, id molestie diam semper et. Nam ultricies ultricies mauris, eget semper nisi tincidunt a. Mauris in magna justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas et enim mi. Sed rhoncus, ipsum et vehicula luctus, metus nunc sodales felis, aliquet finibus ante purus ut magna. Nam nulla quam, gravida ac odio sed, malesuada mollis sem. Nam sagittis nunc quis sodales molestie. Suspendisse mauris mauris, egestas nec nisl vitae, faucibus fermentum mauris. Curabitur vestibulum magna eget augue tincidunt finibus. Suspendisse vel interdum ipsum. Phasellus sapien nibh, accumsan et lacus eu, suscipit volutpat massa.  Vivamus posuere, nulla id pulvinar faucibus, risus sem porta diam, et consectetur odio nisi suscipit massa. Morbi gravida quam in arcu consectetur molestie. Duis est magna, luctus a leo vitae, molestie faucibus odio. Phasellus malesuada diam sed suscipit lobortis. Ut id odio placerat massa volutpat vehicula id et arcu. Nullam ut consequat est. Nunc non volutpat ante. Praesent fringilla justo a sagittis pretium. Aenean varius rhoncus libero ac congue. Morbi id neque quam.'),
(6, 'testdann', 'anotace', 'ttttest'),
(13, 'jhuhu', 'anotace', 'popis');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
('alex', '$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 1),
('mkyong', '$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 1),
('test', '$2a$10$04TVADrR6/SPLBjsK0N30.Jf5fNjBugSACeGv1S69dZALR7lSov0y', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `username`, `role`) VALUES
(2, 'mkyong', 'ROLE_ADMIN'),
(4, 'test', 'ROLE_ADMIN'),
(1, 'mkyong', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `youtube_playlist`
--

CREATE TABLE `youtube_playlist` (
  `youtube_playlist_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `youtube_video`
--

CREATE TABLE `youtube_video` (
  `youtube_video_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `start_at` int(11) NOT NULL DEFAULT '0',
  `caption_language` varchar(10) DEFAULT NULL,
  `is_show_related` char(1) NOT NULL DEFAULT 'Y',
  `is_show_controls` char(1) NOT NULL DEFAULT 'Y',
  `is_privacy_policy` char(1) NOT NULL DEFAULT 'N',
  `is_show_info` char(1) NOT NULL DEFAULT 'Y',
  `is_autoplay` char(1) NOT NULL DEFAULT 'N',
  `is_caption` char(1) NOT NULL DEFAULT 'N',
  `is_allow_fullscreen` char(1) NOT NULL DEFAULT 'Y',
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `youtube_video`
--

INSERT INTO `youtube_video` (`youtube_video_id`, `name`, `width`, `height`, `start_at`, `caption_language`, `is_show_related`, `is_show_controls`, `is_privacy_policy`, `is_show_info`, `is_autoplay`, `is_caption`, `is_allow_fullscreen`, `description`) VALUES
(1, 'LReMcF9YwJM', 350, 0, 0, NULL, 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y', 'kontrafakt podzemi'),
(2, 'rruOu2nIPHc', 250, 0, 0, NULL, 'Y', 'Y', 'N', 'Y', 'N', 'N', 'Y', 'majk spirit - primetime');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`),
  ADD UNIQUE KEY `uni_username_role` (`role`,`username`),
  ADD KEY `fk_username_idx` (`username`);

--
-- Indexes for table `youtube_playlist`
--
ALTER TABLE `youtube_playlist`
  ADD PRIMARY KEY (`youtube_playlist_id`);

--
-- Indexes for table `youtube_video`
--
ALTER TABLE `youtube_video`
  ADD PRIMARY KEY (`youtube_video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `user_role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `youtube_playlist`
--
ALTER TABLE `youtube_playlist`
  MODIFY `youtube_playlist_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `youtube_video`
--
ALTER TABLE `youtube_video`
  MODIFY `youtube_video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
