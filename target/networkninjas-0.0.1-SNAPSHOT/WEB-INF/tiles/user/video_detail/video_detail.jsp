<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"> Video - ${youtubeVideo.description}</div>
  <div class="panel-body">
    <div id="youtube_videoId">${youtubeVideo.youtubeVideoId}</div>
    <div id="youtube_videoName">${youtubeVideo.name}</div>
    
    <c:if test="${not empty iframe}">
    	<div id="youtube_videoIFrame">${iframe}</div>
    </c:if>
     
</div>
	
	
	

		
	
	
		
	

	