<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">List of all videos</div>
  <div class="panel-body">
    <p>This is the list of videos</p>
  </div>

  <!-- Table -->
  <table class="table">
  	<tr> <td> Name </td> <td> Description </td> </tr>
  	<c:forEach var="v" items="${youtubeVideos}">
    	<tr> <td> <a href="/networkninjas/youtubeVideos/${v.youtubeVideoId}"> ${v.name} </a>  </td> <td> ${v.description} </td> </tr>
    </c:forEach>
  </table>
</div>