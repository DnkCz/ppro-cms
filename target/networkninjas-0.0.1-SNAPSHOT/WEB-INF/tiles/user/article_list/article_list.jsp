<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">List of all articles</div>
  <div class="panel-body">
    <p>This is the list of article</p>
     
  </div>

  <!-- Table -->
  <table class="table">
  	<tr> <td> Name </td> <td> Annotation </td> </tr>
  	<c:forEach var="a" items="${articles}">
    	<tr> <td> <a href="/networkninjas/articles/${a.articleId}">  ${a.name} </a> </td> <td> ${a.annotation} </td> </tr>
    </c:forEach>
  </table>
</div>